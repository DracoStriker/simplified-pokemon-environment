# Simplified Pokemon Environment

Paper Link: https://ieeexplore.ieee.org/document/9096092

Please cite if used.

    @INPROCEEDINGS{9096092,
    
      author={D. {Simões} and S. {Reis} and N. {Lau} and L. P. {Reis}},
    
      booktitle={2020 IEEE International Conference on Autonomous Robot Systems and Competitions (ICARSC)}, 
    
      title={Competitive Deep Reinforcement Learning over a Pokémon Battling Simulator}, 
    
      year={2020},
    
      volume={},
    
      number={},
    
      pages={40-45},}
